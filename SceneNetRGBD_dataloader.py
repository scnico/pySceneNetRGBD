import os
import numpy as np
import torch
from PIL import Image
import scenenet_pb2 as sn
import semantic_classes.semantic_class_dictionary_generator as class_dict_gen


class SceneNetRGBDDataset(object):
    def __init__(self, root, transforms, data_index=None, verbose=False, validate=False):
        # Mandatory transforms.
        self.transforms = transforms
        # Dataset root folder.
        self.root = root
        # Size of the loaded dataset.
        self.n_views = 0
        # Container for all loaded trajectories.
        self.trajectories = sn.Trajectories()
        self.verbose = verbose
        self.validate = validate

        # Build the class dictionary to convert class ids to the 13 NYU classes.
        self.class_dict = class_dict_gen.get_dict()

        # Contains all paths to the selected protobuf files.
        self.protobuf_paths = []

        # Get the paths to the selected protobuf files.
        if validate:
            self.protobuf_paths.append(root + '/scenenet_rgbd_val.pb')
        else:
            # Select the parts of the training dataset.
            if data_index == None:
                data_range = range(0,17)
            elif data_index < 0 or 16 < data_index:
                print('Scenenet protobuf data index must be integer and between 0 and 16, but is {0}'.format(data_index))
                return
            else:
                data_range = range(data_index, data_index + 1)

            for i in data_range:
                self.protobuf_paths.append(root + '/scenenet_rgbd_train_' + str(i) + '.pb')

        # Load all trajectories from the specified protobuf paths
        for protobuf_path in self.protobuf_paths:
            # Reads the trajectories from the protobuf file.
            sn_trajectories = sn.Trajectories()
            try:
                with open(protobuf_path,'rb') as f:
                    sn_trajectories.ParseFromString(f.read())
            except IOError:
                print('Scenenet protobuf data not found at location: {0}'.format(data_root_path))

            # Adds the trajectories of this protobuf file to the other previously
            # loaded trajectories.
            self.trajectories.MergeFrom(sn_trajectories)

            # Counts the number of freshly loaded views.
            for traj in sn_trajectories.trajectories:
                self.n_views += np.size(traj.views)





    def __getitem__(self, idx):
        # Extract the index for the trajectory and the view. 300 is the number
        # of views per trajectory.
        idx_traj = int(idx / 300)
        idx_view = int(idx % 300)

        # Get the chosen trajectory and view.
        trajectory = self.trajectories.trajectories[idx_traj]
        view = trajectory.views[idx_view]

        # Container for the dataset instance id and the class labels
        instance_id = []
        instance_labels = []

        # Reads all the instances in this trajectory and extract the instance_id
        # and the label.
        for instance in trajectory.instances:
            instance_id.append(instance.instance_id)
            if instance.semantic_wordnet_id == '':
                instance_labels.append(0)
            else:
                instance_labels.append(self.class_dict[instance.semantic_wordnet_id])

        # Conversion from lists into arrays.
        instance_id = np.array(instance_id)
        instance_labels = np.array(instance_labels)

        # Construct the data paths to the instance image and the camera image.
        render_path = trajectory.render_path
        if self.validate:
            instance_path = os.path.join(self.root, 'val', render_path, 'instance', str(view.frame_num) + '.png')
            image_path = os.path.join(self.root, 'val', render_path, 'photo', str(view.frame_num) + '.jpg')
        else:
            instance_path = os.path.join(self.root, 'train', render_path, 'instance', str(view.frame_num) + '.png')
            image_path = os.path.join(self.root, 'train', render_path, 'photo', str(view.frame_num) + '.jpg')

        # Loads the camera image and the instance image.
        image = Image.open(image_path)
        instance_image = np.array(Image.open(instance_path))


        # Array containing the ids of the instances in the image.
        instances_unique = np.unique(instance_image)

        # Generates the labels and the masks.
        labels = np.zeros(len(instances_unique))
        masks = np.zeros((len(instances_unique), instance_image.shape[0], instance_image.shape[1]))
        for i,inst in enumerate(instances_unique):
            labels[i] = instance_labels[instance_id == inst]
            masks[i, instance_image == inst] = 1

        # Removes mask 0 as this is the 'don't care' area.
        masks = masks[1:]
        labels = labels[1:]

        # Removes too small masks and corresponding labels.
        thr = 1000
        for i in range(len(masks)-1, -1, -1):
            sum_local = np.sum(masks[i,:,:])
            if (sum_local < thr):
                masks = np.delete(masks, i, axis=0)
                labels = np.delete(labels, i, axis=0)








        # NYU_13_CLASSES = ['Unknown', 'Bed', 'Books', 'Ceiling', 'Chair',
        #           'Floor', 'Furniture', 'Objects', 'Picture',
        #           'Sofa', 'Table', 'TV', 'Wall', 'Window'
        #          ]
        #
        # # image.show()
        # import matplotlib.pyplot as plt
        # plt.figure()
        # for i in range(10):
        #     plt.subplot(3,4,i+1)
        #     plt.title(NYU_13_CLASSES[int(labels[i])])
        #     plt.imshow(masks[i,:,:])
        # plt.subplot(3,4,12)
        # plt.imshow(np.array(image))
        # plt.show()





        # Get bounding box coordinates for each mask.
        boxes = np.zeros((len(masks), 4))
        for i in range(len(masks)):
            pos = np.where(masks[i])
            xmin = np.min(pos[1])
            xmax = np.max(pos[1])
            ymin = np.min(pos[0])
            ymax = np.max(pos[0])
            boxes[i,:] = np.array([xmin, ymin, xmax, ymax])

        # Convert everything into a torch.Tensor
        boxes = torch.as_tensor(boxes, dtype=torch.float32)
        labels =  torch.as_tensor(labels, dtype=torch.int64)
        masks = torch.as_tensor(masks, dtype=torch.uint8)
        image_id = torch.tensor([idx])
        area = (boxes[:, 3] - boxes[:, 1]) * (boxes[:, 2] - boxes[:, 0])

        target = {}
        target["boxes"] = boxes
        target["labels"] = labels
        target["masks"] = masks
        target["image_id"] = image_id
        target["area"] = area

        if self.transforms is not None:
            image, target = self.transforms(image, target)

        return image, target

    def __len__(self):
        return self.n_views
